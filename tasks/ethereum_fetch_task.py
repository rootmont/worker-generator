import jsonpickle
import json
import logging
from web3 import Web3, HTTPProvider
from ratelimiter import RateLimiter

from app import db
from config import *
from model.current_work import CurrentWork
from tasks.message_sender import MessageSender


class EthereumFetchTask(object):
    """
    This class basically assumes there is only one of this server.
    """
    def __init__(self):
        self._provider = HTTPProvider('https://mainnet.infura.io/v3/73827c14a02c45a79424714be7fab8eb')
        self._web3 = Web3(self._provider)
        self._rate_limiter = RateLimiter(max_calls=90, period=1)

    def get_latest(self):
        current_work = CurrentWork.get_latest_by_work_name(ETHEREUM_FETCH_BLOCK_QUEUE)

        # Init if None
        ethereum_fetch_block = None
        if current_work is None:
            ethereum_fetch_block = {'block_num': 7390000}
            current_work = CurrentWork(ETHEREUM_FETCH_BLOCK_QUEUE, json.dumps(ethereum_fetch_block))
        else:
            ethereum_fetch_block = json.loads(current_work.data)

        # This looks off but is right.
        ethereum_fetch_block['block_num'] += 1

        current_block_num = ethereum_fetch_block['block_num']
        remote_block_num = self._web3.eth.blockNumber

        for i in range(current_block_num, remote_block_num):
            ethereum_fetch_block['block_num'] = i
            # Put it on the queue
            MessageSender(ETHEREUM_FETCH_BLOCK_QUEUE, ethereum_fetch_block)
            # Update that we sent it
            current_work.data = json.dumps(ethereum_fetch_block)

            if ethereum_fetch_block['block_num'] % 5000 == 0:
                logging.info("Sent message to fetch block: {}".format(ethereum_fetch_block['block_num']))
                db.session.add(current_work)
                db.session.commit()
