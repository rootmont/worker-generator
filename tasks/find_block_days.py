import datetime
import logging
from typing import Union
from random import randrange

from app import db
from config import *
from model.block_info import BlockInfo
from model.ethereum_block_days import EthereumBlockDays
from repository.s3_repository import S3Repository
from tasks.message_sender import MessageSender


class FindEthereumBlockDays(object):
    def __init__(self):
        self._remote_repository = S3Repository()
        # first timestamp of 0 block eth
        self._current_date: datetime.date = datetime.datetime.fromtimestamp(1438226773).date()
        self._start_of_day_block: int = 0
        # 17 second block time -> we'll hop ahead this far
        self._default_interval = 7000
        self.__call__()

    def __call__(self):
        block_day = EthereumBlockDays.get_latest()
        if block_day is not None:
            self._start_of_day_block = block_day.end_block + 1
            self._current_date = block_day.day + datetime.timedelta(days=1)

        while True:
            block_info = self._find_by_quick_sort(lower=self._start_of_day_block, upper=self._start_of_day_block)
            if block_info is None:
                logging.info("Need more blocks...")
                break

            day: datetime.date = datetime.datetime.fromtimestamp(block_info.time_stamp).date()

            # save interval
            ethereum_block_days = EthereumBlockDays(self._start_of_day_block, block_info.block_num, day)
            db.session.add(ethereum_block_days)
            db.session.commit()

            # send interval over the wire
            MessageSender(ETHEREUM_DAYS_QUEUE, {
                'start_block': self._start_of_day_block,
                'end_block': block_info.block_num - 1,
                'day': day.strftime("%m/%d/%Y")
            })

            self._start_of_day_block = block_info.block_num + 1
            self._current_date = day + datetime.timedelta(days=1)

    def _find_by_quick_sort(self, lower: int, upper: int, finding_window: bool = True) -> Union[None, BlockInfo]:
        # Stop infinity
        if lower > upper:
            raise ValueError("lower: {} was greater than upper {}".format(lower, upper))
        if lower + 1 == upper:
            b = self._remote_repository.get_block_info(lower)
            bd = datetime.datetime.fromtimestamp(b.time_stamp).date()
            logging.info("Found between {}: {}, {}: {}".format(lower, self._current_date, upper, bd))
            return b
        if finding_window:
            upper += self._default_interval
            block_info = self._remote_repository.get_block_info(upper)
            # We don't have this block, probably went too far
            if block_info is None:
                result = self._check_backwards(lower, upper)
                if result is None:
                    return None
                else:
                    upper = result
                    self._find_by_quick_sort(lower, upper, False)
            block_info_date: datetime.date = datetime.datetime.fromtimestamp(block_info.time_stamp).date()
            # 0 block is 1970...
            if self._current_date >= block_info_date:
                return self._find_by_quick_sort(lower, upper, True)
            else:
                return self._find_by_quick_sort(lower, upper, False)
        else:
            pivot = randrange(lower, upper)
            pivot_info = self._remote_repository.get_block_info(pivot)
            # We found an upper bound but haven't processed all the blocks between.
            if pivot_info is None:
                return None
            pivot_date: datetime.date = datetime.datetime.fromtimestamp(pivot_info.time_stamp).date()
            if pivot_date > self._current_date:
                upper = pivot
            elif pivot_date <= self._current_date:
                lower = pivot
            return self._find_by_quick_sort(lower, upper, False)

    def _check_backwards(self, lower, upper) -> Union[None, int]:
        # Break out if we get too close, moving upper bound down the numberline linearly
        while upper - lower > 1500:
            upper -= 200
            block_info = self._remote_repository.get_block_info(upper)
            if block_info is not None and\
                    self._current_date >= datetime.datetime.fromtimestamp(block_info.time_stamp).date():
                return None
            elif block_info is not None and\
                    self._current_date < datetime.datetime.fromtimestamp(block_info.time_stamp).date():
                return upper
        return None
