import json
import pika
import pika_pool

from config import *


def declare_queues():
    # declare queues
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=RABBITMQ_HOST,
        credentials=pika.PlainCredentials(RABBITMQ_USER, RABBITMQ_PASS))
    )
    channel = connection.channel()

    channel.queue_declare(queue=ETHEREUM_FETCH_BLOCK_QUEUE, durable=True)
    channel.queue_declare(queue=ETHEREUM_DAYS_QUEUE, durable=True)
    connection.close()


declare_queues()


pool = pika_pool.QueuedPool(
    create=lambda: pika.BlockingConnection(pika.ConnectionParameters(
        host=RABBITMQ_HOST,
        credentials=pika.PlainCredentials(RABBITMQ_USER, RABBITMQ_PASS))),
    max_size=10,
    max_overflow=10,
    timeout=10,
    recycle=3600,
    stale=45,
)


class MessageSender(object):
    def __init__(self, queue: str, obj):
        self._queue: str = queue
        self._obj = obj
        self.__call__()

    def __call__(self):
        with pool.acquire() as cxn:
            cxn.channel.basic_publish(
                body=json.dumps(self._obj),
                exchange=RABBITMQ_EXCHANGE,
                routing_key=self._queue,
                properties=pika.BasicProperties(
                    content_type='application/json',
                    content_encoding='utf-8',
                    delivery_mode=2,
                )
            )
