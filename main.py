import logging

from app import application
from tasks.find_block_days import FindEthereumBlockDays
from tasks.ethereum_fetch_task import EthereumFetchTask


class NoRunningFilter(logging.Filter):
    def filter(self, record):
        return "maximum number of running" not in record.msg


logging.basicConfig(level=logging.INFO)
appschedule_filter = NoRunningFilter()
logging.getLogger("apscheduler.scheduler").addFilter(appschedule_filter)

if __name__ == '__main__':
    # FindEthereumBlockDays()
    application.scheduler.add_job(lambda: EthereumFetchTask().get_latest(),
                                  trigger='interval',
                                  seconds=5,
                                  max_instances=1)
    application.run()
