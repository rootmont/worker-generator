import functools
import logging
import json
from time import sleep
from typing import Union, Callable, List
import datetime
from sqlalchemy.dialects.mysql import insert
from sqlalchemy import func

from app import db


def as_dict(table, row):
    # TODO: implies primary key is "id", maybe we can implicitly define this
    ret = {}
    for c in table.__table__.columns:
        if c.name is not "id":
            type_mutation = getattr(row, c.name)
            if isinstance(getattr(row, c.name), datetime.datetime):
                type_mutation = getattr(row, c.name).strftime('%Y-%m-%d %H:%M:%S')
            elif isinstance(getattr(row, c.name), datetime.date):
                type_mutation = getattr(row, c.name).strftime('%Y-%m-%d')
            ret.update({c.name: type_mutation})
    return ret


def _handle_value(value):
    return "\"" + value + "\"" if isinstance(value, str) else str(value)


def insert_update(table, objects):
    ret = []
    if not isinstance(objects, list):
        objects = [objects]
    for counter, row in enumerate(objects):
        d = as_dict(table, row)
        columns = ', '.join([str(k) for k, _ in d.items()])
        values = ', '.join(["\"" + v + "\"" if isinstance(v, str) else str(v) for _, v in d.items()])
        column_values = ', '.join([k + "=" + _handle_value(v) for k, v in d.items()])
        sql = "INSERT INTO {} ({}) VALUES ({}) ON DUPLICATE KEY UPDATE {}"\
            .format(table.__table__.name, columns, values, column_values)
        res = db.session.execute(sql)
        ret.append(res.lastrowid)
    return ret


def retry(method: Union[Callable, None] = None, retries: int = 5):
    """
    Retries a method if it throws
    :param method: method to wrap
    :param retries: number of retries to try
    :return: wrapped method that retries
    """
    # This recursion will wrap the argument positionally if it is entered.
    if method is None:
        return functools.partial(retry, retries=retries)

    @functools.wraps(method)
    def f(*args, **kwargs):
        ret = None
        for j in range(0, retries):
            try:
                # back off
                sleep(j)
                ret = method(*args, **kwargs)
                # we didn't throw so we're done
                break
            except Exception as e:
                if j == retries - 1:
                    raise e
                else:
                    logging.warning(e)
        return ret
    return f


def send_result_to_queue(method: Union[Callable, None], queue: str):
    # This recursion will wrap the argument positionally if it is entered.
    if method is None:
        return functools.partial(retry, queue=queue)

    if queue is None:
        raise Exception("Queue not defined for a message")

    @functools.wraps(method)
    def f(*args, **kwargs):
        ret = method(*args, **kwargs)
        if ret is not None:
            MessageSender.get_instance().send_message(queue, ret)
        return ret

    return f


def get_headers():
    return {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "cache-control": "max-age=0",
        "scheme": "https",
        "cookie": "__cfduid=d420f92bda370ea83f73a630429ee9d4f1524988388; _xicah=c9961c3a-fca776fa",
        "accept-language": "en-US",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
        "method": "GET",
        "accept-encoding": "",
        "upgrade-insecure-requests": "1",
    }


def get_proxy_d():
    return {"https": "https://107.170.113.28:31380", "http": "http://107.170.113.28:31380"}
