from __future__ import annotations
import datetime
from typing import Union
from sqlalchemy import desc

from app import db


class EthereumBlockDays(db.Model):
    """
    Stores the delineation of ethereum blocks into days.
    """
    def __init__(self, start_block: int, end_block: int, date: datetime.date):
        self.start_block = start_block
        self.end_block = end_block
        self.day = date

    id = db.Column(db.BigInteger, primary_key=True)
    start_block = db.Column(db.Integer, nullable=False)
    end_block = db.Column(db.Integer, nullable=False)
    day = db.Column(db.Date, nullable=False, unique=True)

    @classmethod
    def get_latest(cls) -> Union[EthereumBlockDays, None]:
        return db.session.query(cls).order_by(desc(cls.day)).first()
