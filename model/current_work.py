from __future__ import annotations
import datetime
from typing import Union

from app import db


class CurrentWork(db.Model):
    """
    Stores the blocknumber up to we have sent work for down the pipeline.
    """
    def __init__(self, work_name: str, data: str):
        self.work_name = work_name
        self.data = data
        self.updated = datetime.datetime.now()

    id = db.Column(db.BigInteger, primary_key=True)
    work_name = db.Column(db.String(32), nullable=False)
    data = db.Column(db.Text, nullable=False)
    updated = db.Column(db.DateTime, nullable=False)

    @classmethod
    def get_latest_by_work_name(cls, work_name: str) -> Union[CurrentWork, None]:
        return db.session.query(cls).filter(cls.work_name == work_name).first()
