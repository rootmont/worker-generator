FROM python:3.7

RUN mkdir /app
COPY ./requirements.txt /app
WORKDIR /app
RUN pip install -r requirements.txt

CMD ["python", "main.py"]