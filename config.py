import os

RUNNING_ENV = os.environ.get('RUNNING_ENV', 'local')

DB_NAME = os.environ.get('DB_NAME', 'api_stats')
DB_LOCATION = os.environ.get('DB_LOCATION', 'mysql')
DB_USER = os.environ.get('DB_USER', 'root')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'IFarmLemons')
DB_PORT = os.environ.get('DB_PORT', '3306')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', 'rabbitmq-mgmt')
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASS = os.environ.get('RABBITMQ_PASS', 'guest')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE', '')

AMAZON_ID = os.environ.get('AMAZON_ID', '')
AMAZON_KEY = os.environ.get('AMAZON_KEY', '')
AMAZON_REGION = "us-west-1"
BLOCK_BUCKET = "eth-block-store"

ETHEREUM_FETCH_BLOCK_QUEUE = "ethereum_blocks"
ETHEREUM_DAYS_QUEUE = "ethereum_days"
