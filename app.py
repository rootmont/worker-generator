from flask import Flask, Response
from flask_sqlalchemy import SQLAlchemy

from apscheduler.schedulers.background import BackgroundScheduler

from config import *


class Application:
    def __init__(self):
        self._scheduler = BackgroundScheduler()

        self._app = Flask(__name__)
        # self._app.debug = bool(self._config['debug'])
        if RUNNING_ENV == "local":
            self._app.debug = True
        else:
            self._app.debug = False
        self._app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://' + DB_USER + ':' + DB_PASSWORD + '@' + \
                                                      DB_LOCATION + ':' + DB_PORT + '/' + DB_NAME
        self._app.config['SQLALCHEMY_POOL_SIZE'] = 16
        self._app.config['SQLALCHEMY_RECORD_QUERIES'] = True
        self._app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self._db = SQLAlchemy(self._app)

    def run(self):
        self._scheduler.start()
        self._app.run(host="0.0.0.0", port=5000)

    @property
    def flask_app(self):
        return self._app

    @property
    def db(self):
        return self._db

    @property
    def scheduler(self):
        return self._scheduler


application = Application()
db = application.db


@application.flask_app.route('/health',  methods=['GET'])
def health():
    return Response("{}", status=200, mimetype='application/json')
